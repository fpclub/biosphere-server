-module(bio_server).
-export([start/0, start_sync/0, stop/0]).

start() ->
    application:ensure_all_started(bio_server),
    lager:info("Started"),
    ok.

start_sync() ->
    sync:go(),
    sync:onsync(fun(_) -> bio_server_app:restart_server() end),
    start().

stop() ->
    application:stop(bio_server).

