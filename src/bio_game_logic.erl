-module(bio_game_logic).
-include("bio_game_instance.hrl").

-export([grow_specie/2, migrate_specie/3]).

grow_specie(#specie_state{number = N}, #cell_state{}) ->
    N + N * (1 - N / 100).

migrate_specie(#specie_state{number = N},
               #cell_state{area_type = From}, #cell_state{area_type = To}) ->
    if From == To -> N * 0.01;
       true -> 0
    end.
