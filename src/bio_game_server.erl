-module(bio_game_server).
-behavior(gen_server).
-include("bio_matrix.hrl").
-include("bio_game_instance.hrl").

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-export([start_link/0, stop/0,
         make_instance/0, get_instance/1, get_instances/0, join_instance/2,
         leave_instance/2, destroy_instance/1]).
-export([add_specie/2]).

%
% User interface
%

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

stop() ->
    gen_server:call(?MODULE, stop).

make_instance() ->
    gen_server:call(?MODULE, make_instance).

get_instance(Id) ->
    gen_server:call(?MODULE, {get_instance, Id}).

get_instances() ->
    gen_server:call(?MODULE, get_instances).

join_instance(Player, Instance) ->
    gen_server:call(?MODULE, {join_instance, Player, Instance}).

leave_instance(Player, Instance) ->
    gen_server:cast(?MODULE, {leave_instance, Player, Instance}).

destroy_instance(Id) ->
    gen_server:cast(?MODULE, {destroy_instance, Id}).

add_specie(Spec, InstanceId) ->
    gen_server:cast(?MODULE, {add_specie, Spec, InstanceId}).

%
% gen-server callbacks
%

init([]) -> {ok, #{}}.

handle_call(make_instance, _From, State) ->
    Ref = make_ref(),
    Instance = bio_game_instance:init(),
    erlang:send_after(Instance#instance_state.update_each, self(), 
                      {update, Ref}),
    {reply, Ref, maps:put(Ref, Instance, State)};
handle_call(get_instances, _, State) ->
    {reply, State, State};
handle_call({join_instance, Player, Id}, _From, State) ->
    case maps:find(Id, State) of 
        {ok, Instance} -> NewInstance = bio_game_instance:join(Player, Instance),
                          {reply, NewInstance, maps:update(Id, NewInstance, State)};
        error -> {noreply, State}
    end;
handle_call({get_instance, Id}, _From, State) ->
    case maps:find(Id, State) of
        {ok, Instance} -> {reply, Instance, State};
        error -> {noreply, State}
    end;
handle_call(stop, _From, State) ->
    {stop, normal, shutdown_ok, State}.

handle_cast({leave_instance, Player, Id}, State) ->
    case maps:find(Id, State) of
        {ok, Instance} -> NewInstance = bio_game_instance:leave(Player, Instance),
                          {noreply, maps:update(Id, NewInstance, State)};
        error -> {noreply, State}
    end;
handle_cast({destroy_instance, Id}, State) ->
    case maps:find(Id, State) of
        {ok, Instance} -> ok = bio_game_instance:terminate(Instance),
                          {noreply, maps:remove(Id, State)};
        error -> {noreply, State}
    end;
handle_cast({add_specie, Spec, Id}, State) ->
    case maps:find(Id, State) of
        {ok, Instance=#instance_state{map = Map}} ->
            X = random:uniform(Map#matrix.width),
            Y = random:uniform(Map#matrix.height),
            NewInstance = bio_game_instance:add_specie(Instance, X, Y, Spec),
            {noreply, maps:update(Id, NewInstance, State)};
        error -> 
            {noreply, State}
    end;
handle_cast(_, State) ->
    {noreply, State}.

handle_info({update, Id}, State) ->
    case maps:find(Id, State) of
        {ok, Instance} -> NewInstance = bio_game_instance:update(Instance),
                          erlang:send_after(NewInstance#instance_state.update_each,
                                            self(), {update, Id}),
                          maps:fold(fun (Name, _, []) ->
                                            bio_login_server:forward(Name, {update, NewInstance}),
                                            []
                                    end,
                                    [],
                                    NewInstance#instance_state.players),
                          lager:info("Updated instance ~w~n", [Id]),
                          {noreply, maps:update(Id, NewInstance, State)};
        error -> {noreply, State}
    end;
handle_info(Msg, State) ->
    lager:info("Unexpected message: ~p~n", [Msg]),
    {noreply, State}.

terminate(normal, _State) ->
    ok.

code_change(_, _State, _) ->
    ok.    

