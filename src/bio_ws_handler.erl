-module(bio_ws_handler).
-behavior(cowboy_websocket_handler).

-export([init/3,
         websocket_init/3, websocket_handle/3,
         websocket_info/3, websocket_terminate/3]).

-export([error_reply/2]).

init(_, _Req, _Opts) ->
    {upgrade, protocol, cowboy_websocket}.

%
% Message parsing
%

parse(Text) ->
    try
        Json = jiffy:decode(Text, [return_maps]),
        {ok, Json}
    catch
        Error={error, _} -> Error
    end.

reply({Json, State}, Request) ->
    {reply, {text, jiffy:encode(Json)}, Request, State}.

error_reply(Error, State) -> {#{type => error, error => Error}, State}.
                            
websocket_init(_, Req, _Opts) ->
    self() ! lazy_init,
    {ok, Req, no_data}.

websocket_handle({text, Request}, Req, State) ->
    case parse(Request) of
        {ok, Json} -> reply(handle_json(Json, State), Req);
        {error, _} -> reply(error_reply(<<"Invalid JSON">>, State), Req)
    end;
websocket_handle(_, Req, State) ->
    reply(error_reply(<<"Invalid request">>, State), Req).

websocket_info(lazy_init, Req, _Data) ->
    {ok, Req, bio_protocol:init()};
websocket_info(Msg, Req, State) ->
    case bio_protocol:handle(State, Msg) of
        {reply, Json, NewState} -> reply({Json, NewState}, Req);
        {noreply, NewState} -> {ok, Req, NewState}
    end.

websocket_terminate(_Reason, _Req, State) ->
    bio_protocol:terminate(State).

handle_json(Json, State) ->
    case Json of
        #{<<"type">> := Type} ->
            bio_protocol:handle(State, Type, Json);
        _ -> error_reply(<<"Request has no type">>, State)
    end.         
