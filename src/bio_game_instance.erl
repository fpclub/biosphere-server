-module(bio_game_instance).
-include("bio_keyset.hrl").
-include("bio_game_instance.hrl").
-export([init/0, join/2, update/1, add_specie/4, leave/2, terminate/1]).
-export([map_layers/2]).

map_ground() ->
    [[w, w, w, g, g, g, w, w, w, w, w, w, w, w, w, w, w, w, w, w],
     [w, w, g, g, g, g, g, g, w, w, w, w, g, g, g, g, g, g, g, w],
     [w, g, g, g, g, g, g, g, w, w, w, g, g, g, g, g, g, g, g, w],
     [w, g, g, g, g, g, g, g, g, w, g, g, g, g, g, g, g, g, g, w],
     [w, g, g, g, g, g, g, g, g, w, g, g, g, g, g, g, g, g, w, w],
     [w, g, g, g, g, g, g, g, w, w, w, g, g, g, g, g, g, w, w, w],
     [w, g, g, g, g, g, g, w, w, w, w, w, g, g, g, g, w, w, w, w],
     [w, w, g, g, g, g, w, w, w, w, g, g, w, w, w, w, w, w, w, w],
     [w, w, w, g, w, w, w, w, w, w, g, g, g, w, w, w, w, w, w, w],
     [w, w, w, g, g, g, w, w, w, w, g, g, g, g, g, w, w, w, w, w],
     [w, w, g, g, g, g, w, w, w, w, g, g, g, g, g, g, w, w, w, w],
     [w, g, g, g, g, g, w, w, w, w, g, g, g, g, g, g, w, w, w, g],
     [w, g, g, g, g, g, w, w, w, w, g, g, g, g, g, g, w, w, g, g],
     [w, g, g, g, g, w, w, w, w, w, g, g, g, g, g, g, w, g, g, g],
     [w, g, g, g, g, w, w, w, w, w, w, g, g, g, g, w, w, g, g, g],
     [w, g, g, g, g, w, w, w, w, w, w, w, g, g, g, w, w, g, g, g],
     [w, w, g, g, g, w, w, w, w, w, w, w, w, g, g, w, w, g, g, g],
     [w, w, w, g, g, w, w, g, g, g, g, g, w, w, w, w, w, g, g, g],
     [w, w, w, w, w, w, g, g, g, g, g, g, g, g, w, w, w, w, w, w],
     [w, w, w, w, w, g, g, g, g, g, g, g, g, g, g, w, w, w, w, w]].

map_temp() ->
    [[-5, -10, -10, -12, -20, -12,  -5, -10, -15, -20, -30, -30, -40, -40, -50, -40, -40, -30, -20, -20],
     [-5,  -5,  -8, -10, -10, -15, -10,  -8, -15, -25, -15, -10, -30, -50, -70, -50, -40, -30, -30, -20],
     [-5,  -8, -15, -20, -20, -20, -15, -10,  -8,  -5,  -8, -10, -20, -30, -40, -40, -30, -30, -20, -15],
     [ 0,   0,  -2,  -5,  -8,  -8,  -5,  -5,  -2,   0,  -2,  -5,  -5, -15, -25, -15,  -5,  -2,  -2,   0],
     [ 0,   0,   0,   5,   5,  10,  15,  10,   5,   0,   0,  -5, -10, -15, -20, -15, -10, -10,   0,   0],
     [ 5,   3,   5,  10,  20,  10,  10,  10,  15,  10,  10,   2,  -2,  -4,  -8, -10, - 5,  -5,  -2,   0],
     [10,   8,  10,  15,  25,  15,  10,  15,  20,  20,  10,  10,  10,  12,  10,   6,  -5,  -2,   0,   5],
     [10,  12,  15,  25,  35,  25,  20,  25,  30,  30,  30,  30,  25,  25,  20,  15,  15,  10,  10,  10],
     [15,  20,  25,  25,  25,  25,  30,  35,  35,  35,  40,  35,  30,  25,  25,  20,  15,  10,  10,  15],
     [15,  25,  35,  30,  20,  20,  30,  40,  40,  40,  40,  40,  35,  35,  30,  25,  25,  20,  20,  25],
     [25,  30,  30,  30,  25,  25,  35,  40,  40,  40,  40,  45,  40,  40,  35,  30,  30,  30,  30,  35],
     [15,  12,  25,  25,  30,  30,  40,  35,  38,  40,  45,  50,  55,  50,  45,  30,  25,  30,  35,  35],
     [15,  12,  30,  30,  30,  35,  40,  35,  35,  35,  30,  45,  50,  40,  30,  25,  20,  35,  40,  40],
     [10,  12,  35,  30,  20,  30,  25,  30,  30,  30,  35,  35,  40,  35,  30,  25,  20,  30,  35,  40],
     [10,  10,  30,  22,  15,  20,  10,  15,  20,  22,  25,  30,  35,  30,  20,  20,  20,  22,  25,  35],
     [ 5,   5,  20,  15,  10,  10,   0,   0,   0,   0,   5,  25,  20,  20,  20,  15,  10,  15,  25,  30],
     [ 0,   0,  10,   2,   0,   0,  -5, -10, -15, -15, -10,  -5,  10,  15,  10,  10,   5,  10,  15,  20],
     [-5,  -5,   0,  -5,   0,  -5, -10, -20, -25, -30, -25, -15, -15,   0,   0,   0,   2,   5,  10,  10],
     [-5,  -5,  -5, -10, -10, -15, -22, -30, -40, -45, -45, -30, -25, -10,  -5,  -5,   0,   0,   0,   0],
     [-8,  -5, -10, -15, -20, -25, -30, -40, -45, -50, -50, -40, -40, -30, -20, -10,  -5,  -5,  -5, -5]].

make_map() ->
    GroundMap = bio_matrix:from_list(map_ground()),
    TempMap = bio_matrix:from_list(map_temp()),
    Species = bio_keyset:from_list([], #specie_state.name),
    bio_matrix:merge(
      fun({G, T}) ->
              #cell_state{area_type = case G of
                                          w -> water;
                                          g -> ground
                                      end,
                          temperature = T,
                          species = Species}
      end, GroundMap, TempMap).

cell_layer(#cell_state{area_type = Type}, <<"ground">>) ->
    Type;
cell_layer(#cell_state{temperature = T}, <<"temperature">>) ->
    T;
cell_layer(#cell_state{species = Species}, <<"Specie ", Name/binary>>) ->
    case bio_keyset:find(Name, Species) of
        #specie_state{number = Number} -> Number;
        _ -> 0
    end;
cell_layer(_, _) ->
    undefined.

map_layer(Map, Layer) ->
    #map_layer{name = Layer,
               content = bio_matrix:to_list(
                           bio_matrix:map(fun(X) -> cell_layer(X, Layer)
                                          end, Map))}.

map_layers(#instance_state{map = Map}, Layers) -> 
    [map_layer(Map, X) || X <- Layers].

init() ->
    #instance_state{map = make_map(), age = 0}.

join(Name, Instance=#instance_state{players = Players}) ->
    Instance#instance_state{players = maps:put(Name, #player_state{}, Players)}.

update(Instance = #instance_state{age = Age, map = Map}) ->
    Grown = bio_matrix:map(fun growCell/1, Map),
    Migrated = migrateMap(Grown),
    Instance#instance_state{age = Age + 1, map = Migrated}.

growCell(Cell = #cell_state{species = Species}) ->
    NewSpecies = bio_keyset:map(
                   fun(Spec) ->
                           NewNumber = bio_game_logic:grow_specie(Spec, Cell),
                           Spec#specie_state{number = NewNumber}
                   end, Species),
    Cell#cell_state{species = NewSpecies}.

%
% Migration
%

migrateTo(From, To) ->
    Species = From#cell_state.species,
    bio_keyset:map(
      fun(S) -> Number = bio_game_logic:migrate_specie(S, From, To),
                S#specie_state{number = Number}
      end, Species).

merge_species(S1 = #specie_state{number = N1},
              #specie_state{number = N2}) ->
    S1#specie_state{number = N1 + N2}.

cellMigration(Cell, Neighbours) ->
    Migrations = [{NDir, migrateTo(Cell, NCell)}
                  || {NDir, NCell} <- Neighbours],
    AntiMigrations =
        lists:map(
          fun({_, Species}) ->
                  bio_keyset:map(
                    fun(S = #specie_state{number = Number}) ->
                            S#specie_state{number = -Number}
                            end, Species)
          end, Migrations),
    {Cell#cell_state{species = bio_keyset:merge(fun merge_species/2,
                                                [Cell#cell_state.species |
                                                 AntiMigrations])},
     Migrations}.

findOppositeDirection(Dir, List) ->
    Opp = case Dir of
              up -> down;
              down -> up;
              left -> right;
              right -> left
          end,
    case lists:keyfind(Opp, 1, List) of
        {_, Species} -> Species;
        _ -> []
    end.

forwardMigration({Cell, _}, Neighbours) ->
    NewSpecies = [findOppositeDirection(Dir, Migrations)
                  || {Dir, {_, Migrations}} <- Neighbours],
    Cell#cell_state{species = bio_keyset:merge(fun merge_species/2,
                                               [Cell#cell_state.species |
                                                NewSpecies])}.

migrateMap(Map) ->
    Migration = bio_matrix:map_neighbours(fun cellMigration/2, Map),
    bio_matrix:map_neighbours(fun forwardMigration/2, Migration).

%
% Migration end
%

add_specie(Instance = #instance_state{map = Map}, X, Y, Specie) ->
    NewMap = bio_matrix:update(
               fun(Cell = #cell_state{species = Species}) ->
                       NewSpecies = bio_keyset:insert(Specie, Species),
                       Cell#cell_state{species = NewSpecies} 
               end,
               Map, X, Y),
    Instance#instance_state{map = NewMap}.

leave(Name, Instance) ->
    Instance.

terminate(Instance) ->
    ok.
