-module(bio_notfound_handler).
-behavior(cowboy_http_handler).

-export([init/3, handle/2, terminate/3]).

init({tcp, http}, Req, []) ->
    {ok, Req, []}.

handle(Req, []) ->
    {ok, Req2} = cowboy_req:reply(404, [],
                                  <<"<h1>404 Page Not Found</h1>">>, 
                                  Req),
    {ok, Req2, []}.

terminate(_, _, _) -> ok.
    
