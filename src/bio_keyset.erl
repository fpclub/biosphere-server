-module(bio_keyset).
-include("bio_keyset.hrl").
-export([from_list/2, from_list/3, to_list/1]).
-export([find/2, insert/2, update/3, merge/3, merge/2, filter/2, map/2]).

remove_duplicates([], _N, _Merger) ->
    [];
remove_duplicates(List = [_], _N, _Merger) ->
    List;
remove_duplicates([First, Second | Tail], N, Merger)
  when element(N, First) == element(N, Second) ->
    remove_duplicates([Merger(First, Second) | Tail], N, Merger);
remove_duplicates([Head | Tail], N, Merger) ->
    [Head | remove_duplicates(Tail, N, Merger)].

discard_second(First, _) -> First.

from_list(List, Key) -> from_list(List, Key, fun discard_second/2).

from_list(List, Key, Merger) ->
    #keyset{key = Key,
            set = remove_duplicates(lists:keysort(Key, List),
                                    Key, Merger)}.

to_list(#keyset{set = List}) ->
    List.

do_find(_Key, _N, []) ->
    false;
do_find(Key, N, [Item | _]) when element(N, Item) > Key ->
    false;
do_find(Key, N, [Item | _]) when element(N, Item) == Key ->
    Item;
do_find(Key, N, [_ | Tail]) ->
    do_find(Key, N, Tail).

find(Key, #keyset{key = N, set = List}) ->
    do_find(Key, N, List).
    
do_insert(Item, _N, []) ->
    [Item];
do_insert(Item, N, List = [Head | _]) when element(N, Item) <
                                           element(N, Head) ->
    [Item | List];
do_insert(Item, N, [Head | _]) when element(N, Item) ==
                                    element(N, Head) ->
    error(already_exists);
do_insert(Item, N, [Head | Tail]) ->
    [Head | do_insert(Item, N, Tail)].

insert(Item, Set = #keyset{key = N, set = List}) ->
    Set#keyset{set = do_insert(Item, N, List)}.

do_update(_Fun, _Key, _N, []) ->
    error(no_such_item);
do_update(_Fun, Key, N, [Head | _]) when element(N, Head) > Key ->
    error(no_such_item);
do_update(Fun, Key, N, [Head | Tail]) when element(N, Head) == Key ->
    [Fun(Head) | Tail];
do_update(Fun, Key, N, [Head | Tail]) ->
    [Head | do_update(Fun, Key, N, Tail)].

update(Fun, Key, Set = #keyset{key = N, set = List}) ->
    Set#keyset{set = do_update(Fun, Key, N, List)}.

do_merge(_Fun, [], List, _N) ->
    List;
do_merge(_Fun, List, [], _N) ->
    List;
do_merge(Fun, [H1 | T1], L2 = [H2 | _], N) when element(N, H1) <
                                                element(N, H2) ->
    [H1 | do_merge(Fun, T1, L2, N)];
do_merge(Fun, [H1 | T1], [H2 | T2], N) when element(N, H1) ==
                                            element(N, H2) ->
    [Fun(H1, H2) | do_merge(Fun, T1, T2, N)];
do_merge(Fun, L1, [H2 | T2], N) ->
    [H2 | do_merge(Fun, L1, T2, N)].

merge(Fun,
      Set1 = #keyset{key = N, set = L1}, #keyset{key = N, set = L2}) ->
    Set1#keyset{set = do_merge(Fun, L1, L2, N)}.
    
merge(_Fun, []) -> #keyset{};
merge(_Fun, [Set]) -> Set;
merge(Fun, [Set | Sets]) ->
    lists:foldl(fun(Set2, Set1) -> merge(Fun, Set1, Set2) end, Set, Sets).

filter(Fun, Set = #keyset{set = List}) ->    
    Set#keyset{set = lists:filter(Fun, List)}.

map(Fun, Set = #keyset{set = List}) ->
    Set#keyset{set = lists:map(Fun, List)};
map(_, Set) ->
    error({wrong_keyset, Set}).

    
