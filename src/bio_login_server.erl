-module(bio_login_server).
-behavior(gen_server).
-include("bio_login_server.hrl").

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-export([start_link/0, stop/0, login/0, logged_in/0, forward/2, logout/1]).

%
% User interface
%

start_link() -> 
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

stop() ->
    gen_server:call(?MODULE, stop).

login() ->
    gen_server:call(?MODULE, login).

logged_in() ->
    gen_server:call(?MODULE, logged_in).

forward(User, Message) ->
    gen_server:cast(?MODULE, {forward, User, Message}).

logout(Name) ->
    gen_server:cast(?MODULE, {logout, self(), Name}),
    ok.

%
% gen-server callbacks
%

init([]) ->
    {ok, #login_server_state{}}.

handle_call(login, {Pid, _},
            State=#login_server_state{next_id = Id, users = Users, refs = Refs}) ->
    Name = list_to_binary(
             lists:concat(["Anonymous ", integer_to_list(Id)])),
    Ref = erlang:monitor(process, Pid),
    lager:info("Logged in as ~p~n", [Name]),
    {reply, Name,
     State#login_server_state{next_id = Id + 1,
                              users = maps:put(Name, Pid, Users),
                              refs = maps:put(Ref, Name, Refs)}};
handle_call(logged_in, _, State=#login_server_state{users = Users}) ->
    {reply, Users, State};
handle_call(stop, _From, State) ->
    {stop, normal, shutdown_ok, State}.

handle_cast({forward, To, Msg},
            State=#login_server_state{users = Users}) ->
    case maps:find(To, Users) of
        {ok, Pid} -> Pid ! Msg;
        _ -> ok
    end,
    {noreply, State};
handle_cast({logout, From, Name},
            State=#login_server_state{users = Users}) ->
    NewUsers = case maps:find(Name, Users) of
                   {ok, From} -> maps:remove(Name, Users);
                   _ -> Users
               end,
    lager:info("~p logged out~n", [Name]),
    {noreply, State#login_server_state{users = NewUsers}}.

handle_info({'DOWN', Ref, process, _, _},
            State = #login_server_state{users = Users, refs = Refs}) ->
    case maps:find(Ref, Refs) of
        {ok, Name} -> NewUsers = maps:remove(Name, Users),
                      NewRefs = maps:remove(Ref, Refs),
                      lager:info("~p is down", [Name]),
                      {noreply, State#login_server_state{users = NewUsers, refs = NewRefs}};
        error -> {noreply, State}
    end;
handle_info(Msg, State) ->
    lager:info("Unexpected message: ~p~n", [Msg]),
    {noreply, State}.

terminate(normal, _State) ->
    ok.

code_change(_, _State, _) ->
    ok.
