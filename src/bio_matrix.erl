-module(bio_matrix).
-include("bio_matrix.hrl").
-export([from_list/1, to_list/1, merge/3, at/3, update/4]).
-export([foreach/2, map/2, any/2]).
-export([map_neighbours/2]). 

from_list([]) ->
    #matrix{width = 0, height = 0, content = []};
from_list(List) ->
    #matrix{width = length(hd(List)),
            height = length(List),
            content = List}.

to_list(#matrix{content = C}) ->
    C.

merge(Fn, #matrix{content = CX}, #matrix{content = CY}) ->
    Content = [[Fn(X) || X <- lists:zip(XS, YS)]
               || {XS, YS} <- lists:zip(CX, CY)],
    from_list(Content).

at(#matrix{content=Matrix}, X, Y) ->
    lists:nth(X, lists:nth(Y, Matrix)).

update(Fn, Matrix = #matrix{content = C}, X, Y) ->
    {RowsBefore, [Row | RowsAfter]} = lists:split(Y - 1, C),
    {CellsBefore, [Cell | CellsAfter]} = lists:split(X - 1, Row),
    NewCell = Fn(Cell),
    NewRow = lists:append(CellsBefore, [NewCell | CellsAfter]),
    NewMatrix = lists:append(RowsBefore, [NewRow | RowsAfter]),
    Matrix#matrix{content = NewMatrix}.

map(Fn, Matrix = #matrix{content = C}) ->
    Matrix#matrix{content = [[Fn(X) || X <- Row] || Row <- C]}.

foreach(Fn, #matrix{content = C}) ->
    lists:foreach(fun(Row) -> lists:foreach(Fn, Row) end, C).

any(Fn, #matrix{content = C}) ->
    lists:any(fun(Row) -> lists:any(Fn, Row) end, C).

map_indices(Fn, #matrix{width = Width,
                        height = Height,
                        content = C}) ->
    lists:map(fun({Y, Row}) ->
                      lists:map(fun({X, Cell}) -> Fn(X, Y, Cell) end,
                                lists:zip(lists:seq(1, Width), Row))
              end, lists:zip(lists:seq(1, Height), C)).

neighbours(X, Y, Matrix = #matrix{width = Width,
                                  height = Height}) ->
    Left = if X == 1 -> [];
               true -> [{left, at(Matrix, X - 1, Y)}]
            end,
    Right = if X == Width -> [];
               true -> [{right, at(Matrix, X + 1, Y)}]
            end,
    Upper = if Y == 1 -> [];
               true -> [{up, at(Matrix, X, Y - 1)}]
            end,
    Beyound = if Y == Height -> [];
                 true -> [{down, at(Matrix, X, Y + 1)}]
              end,
    lists:append([Upper, Right, Beyound, Left]).

map_neighbours(Fn, Matrix) ->
    NewContent = map_indices(
                   fun(X, Y, Cell) ->
                           Fn(Cell, neighbours(X, Y, Matrix))
                   end, Matrix),
    Matrix#matrix{content = NewContent}.
