-module(bio_protocol).
-include("bio_protocol.hrl").
-include("bio_game_instance.hrl").
-export([init/0, terminate/1, handle/2, handle/3, get_instance/1]).

reply(State, Type) -> {#{type => Type}, State}.
reply_error(State, Error) -> reply(State, error, #{error => Error}).
reply(State, Type, Args) -> {Args#{type => Type}, State}.

init() ->
    Name = bio_login_server:login(),
    Instance = bio_game_server:make_instance(),
    bio_game_server:join_instance(Name, Instance),
    bio_game_server:add_specie(#specie_state{name = <<"Sample">>, number = 10}, Instance),
    #bio_state{name = Name, instance = Instance}.

terminate(#bio_state{name = Name, instance = Id}) ->
    bio_login_server:logout(Name),
    bio_game_server:destroy_instance(Id),
    ok.

handle(State, Type, Args) ->
    case Type of
        <<"ping">> -> reply(State, pong);
        <<"require">> ->
            handle_request(State, Args);
        _ -> reply_error(State, <<"Unknown request type: ", Type/binary>>)
    end.

layer_to_json(#map_layer{name = Name, content = Content}) ->
    #{name => Name, content => Content}.

handle_request(State, Args) ->
    case maps:find(<<"what">>, Args) of
        {ok, <<"map">>} ->
            case maps:find(<<"layers">>, Args) of
                {ok, Layers} ->
                    Instance = bio_game_server:get_instance(State#bio_state.instance),
                    reply(State, map, #{layers => 
                                            [layer_to_json(X) ||
                                                X <- bio_game_instance:map_layers(Instance, Layers)]});
                error ->
                    reply_error(State, <<"Missing required field 'layers'">>)
            end;
        {ok, <<"name">>} ->
            reply(State, name, #{name => State#bio_state.name});
        error ->
            reply_error(State, <<"Missing required field 'what'">>)
    end.

handle(State, {update, #instance_state{age = Age}}) ->
    {reply, #{type => update, age => Age}, State};
handle(State = #bio_state{instance = Id}, {get_instance, Pid, Ref}) ->
    Pid ! {Ref, Id},
    {noreply, State};
handle(State, _) ->
    {noreply, State}.

get_instance(Pid) ->
    Ref = make_ref(),
    Pid ! {get_instance, self(), Ref},
    receive {Ref, Id} ->
            Id
    after 5000 -> error(timeout)
    end.
            
