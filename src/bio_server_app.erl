-module(bio_server_app).
-behaviour(application).

-export([start/2, stop/1]).
-export([start_server/0, stop_server/0, restart_server/0]).

routes() ->
    cowboy_router:compile(
      [{'_', [{"/", cowboy_static, {file, "priv/index.html"}},
              {"/js/[...]", cowboy_static, {dir, "priv/js",
                                            [{mimetypes, cow_mimetypes, all}]}},
              {"/css/[...]", cowboy_static, {dir, "priv/css",
                                             [{mimetypes, cow_mimetypes, all}]}},
              {"/play", bio_ws_handler, []},
              {'_', bio_notfound_handler, []}]}]).

start_server() ->
    Port = 8000,
    {ok, _} = cowboy:start_http(bio_http_listener, 100,
                                [{port, Port}],
                                [{env, [{dispatch, routes()}]}]).
    
stop_server() -> 
    cowboy:stop_listener(bio_http_listener),
    bio_login_server:stop(),    
    bio_game_server:stop(),
    timer:sleep(10),
    ok.

restart_server() -> stop_server(), start_server().

start(_StartType, _StartArgs) ->
    start_server(),
    bio_server_sup:start_link().

stop(_State) -> stop_server(), ok.

    
    
