function makeEventHandler() {
    return {
        _callbacks: [],

        push: function(callback) {
            this._callbacks.push(callback);
        },

        handle: function(data) {
            this._callbacks.forEach(function(callback) {
                callback(data);
            });
        }
    };
}
