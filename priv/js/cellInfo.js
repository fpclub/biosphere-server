function bindCellInfo(elementId, map) {
    var element = $(elementId)
    element.empty()

    $('<h2>Cell</h2>').appendTo(element)

    _makeCellRow(element, "At:", "value coords")
    _makeCellRow(element, "Type:", "value area-type")
    _makeCellRow(element, "Temperature:", "value temperature")

    $('<h2>Species</h2>').appendTo(element)
    
    _makeCellRow(element, "Sample:", "value sample-specie")

    map.onMouseEnter(function(data) {
        var x = data.x
        var y = data.y

        $(elementId + " .coords").text("(" + x + ", " + y + ")");
        $(elementId + " .area-type").text(data.map.at(x, y).ground)
        $(elementId + " .temperature").text(data.map.at(x, y).temperature)
        $(elementId + " .sample-specie").text(data.map.at(x, y)["Specie Sample"].toFixed(2))
    });

    map.onMouseLeave(function(data) {
        $(elementId + " .coords").text("");
    });
}

function _makeCellRow(element, name, classes) {
    var label = $('<div>')
    label.attr("class", "label")
    label.text(name)
    label.appendTo(element)

    var value = $('<div>')
    value.attr("class", classes)
    value.text("")
    value.appendTo(element)

    $('<br/>').appendTo(element)
}
