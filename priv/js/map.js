function makeMap(element) {
    var map = {
        _element: element,
        _clickedAt: null,
        _species: {},
        _content: [],
        _onEnter: makeEventHandler(),
        _onLeave: makeEventHandler(),

        ref: function() {
            return $(this._element).data();
        },

        onMouseEnter: function(callback) {
            this._onEnter.push(callback);
        },

        onMouseLeave: function(callback) {
            this._onLeave.push(callback);
        },

        _layers: {},

        showLayer: function(name) {
            this._layers[name].hidden = false
            drawMap(this)
        },

        hideLayer: function(name) {
            this._layers[name].hidden = true
            drawMap(this)
        },

        swapLayer: function(name) {
            this._layers[name].hidden = !this._layers[name].hidden
            drawMap(this)
        },

        addLayer: function(name, callback, weight, isHidden) {
            this._layers[name] = {callback: callback, weight: weight, hidden: isHidden};
        },

        removeLayer: function(name) {
            delete this._layers[name]
        },

        at: function(x, y) {
            return this._content[y][x];
        }
    };

    wsHandle("map", function(data) { _onMapLoad(map.ref(), data)});
    wsSend({type: "require",
            what: "map",
            layers: ["ground", "temperature", "Specie Sample"]})

    $(map._element).data(map);

    map.addLayer("ground", groundColor, 1, false);
    map.addLayer("temperature", tempColor, 2, true);
    map.addLayer("specie", specieColor, 2, false);

    return map;
}

function groundColor(content) {
    if(content.ground == "water")
        return [0, 0, 1];

    return [0.65, 0.65, 0.03];
}

function tempColor(content) { 
    var temp = content.temperature
    var weight = (temp - (-70)) / 130

    return _mixColors(weight, [1, 0.0, 0], [0.0, 0.0, 1]);
}

function specieColor(content, map) {
    var max = map._species["Sample"].max

    return _mixColors(content["Specie Sample"] / max, [0, 1, 0],
                      [0, 0, 0])
}

function drawMap(map) {
    var table = $('<table style="padding: 0px; margin: 0px; border-collapse: collapse">');

    for(var i=0;i<map._content[0].length;i++)
        _makeMapRow(map, i, map._content[i], map._content.length).appendTo(table);

    $(map._element).empty();
    $(map._element).append(table);

    if(map._clickedAt != null) {
        var clicked = map._clickedAt
        var x = clicked.attr("x"); var y = clicked.attr("y")
        clicked = $(map._element + " " +
                    "tr:nth-child(" + (parseInt(y) + 1) + ") " + 
                    "td:nth-child(" + (parseInt(x) + 1) + ")")

        map._clickedAt = null
        _onMapMouseMove(clicked, map._element);
        map._clickedAt = clicked;
    }
}

function _onMapLoad(map, data) {
    data.layers.forEach(_loadMapLayer, map);
    drawMap(map);
}

function _layerMax(layer) {
    var max = 0

    for(var i=0;i<layer.content.length;i++)
        for(var j=0;j<layer.content[i].length;j++)
            if(layer.content[i][j] > max)
                max = layer.content[i][j]

    return max
}

function _loadMapLayer(layer) {
    var map = this;

    if(layer.name.substring(0, 6) == "Specie") {
        if(map._species[layer.name.substring(7)] == undefined)
            map._species[layer.name.substring(7)] = {}

        map._species[layer.name.substring(7)].max = _layerMax(layer)
    }

    function ensureHaveRow(i) {
        if(map._content[i] == undefined)
            map._content[i] = []
    }
    function ensureHaveCell(i, j) {
        ensureHaveRow(i)
        if(map._content[i][j] == undefined)
            map._content[i][j] = {}
    }

    for(var i=0;i<layer.content.length;i++) {
        var row = layer.content[i]

        for(var j=0;j<row.length;j++) {
            ensureHaveCell(i, j)
            map._content[i][j][layer.name] = layer.content[i][j]
        }
    }
}

function _mapColor(map, content) {
    var weightedColors = [0, 0, 0]
    var totalWeight = 0

    var layers = map._layers
    for(var index in layers) {
        if(layers[index].hidden)
            continue

        var weight = layers[index].weight
        var color = layers[index].callback(content, map)

        totalWeight += weight
        
        for(var i=0;i<3;i++)
            weightedColors[i] += color[i] * weight
    }

    for(var i=0;i<3;i++)
        weightedColors[i] /= totalWeight

    return weightedColors
}

function _makeMapRow(map, index, content, size) {
    var row = $('<tr>');

    for(var i=0;i<content.length;i++)
        _makeMapCell(map, i, index, content[i], content.length, size).appendTo(row)

    return row;
}

function _makeMapCell(map, x, y, content, xSize, ySize) {
    var cell = $('<td style="padding: 0px">')

    var tableWidth = $(window).width() * 0.4;

    cell.attr("x", x);
    cell.attr("y", y);
    cell.attr("width", tableWidth / xSize + "px");
    cell.attr("height", tableWidth / ySize + "px");

    cell.attr("onmousemove", "_onMapMouseMove($(this), \"" + map._element + "\")");
    cell.attr("onmouseout", "_onMapMouseOut($(this), \"" + map._element + "\")");
    cell.attr("onclick", "_onMapMouseClick($(this), \"" + map._element + "\")");

    var color = _toHex(_mapColor(map, content))
    cell.attr("bgcolor", color);
    cell.attr("default-color", color);

    cell.append($('<div class="map-border">'));

    return cell;
}

function _mixColors(weight, c1, c2) {
    var color = []

    for(var i=0;i<3;i++)
        color.push(c1[i] * weight + c2[i] * (1 - weight));

    return color;
}

function _toHex(rgb) {
    var red = rgb[0] * 255
    var green = rgb[1] * 255
    var blue = rgb[2] * 255

    if(red > 255) red = 255;
    if(green > 255) green = 255;
    if(blue > 255) blue = 255;

    var rgb = blue | (green << 8) | (red << 16);
    return '#' + (0x1000000 + rgb).toString(16).slice(1)
}

function _toRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return [parseInt(result[1], 16) / 256, parseInt(result[2], 16) / 256, parseInt(result[3], 16) / 256];
}

function _onMapMouseClick(element, mapId) {
    var map = $(mapId).data()
    if(map._clickedAt != null) {
        var clicked = map._clickedAt
        map._clickedAt = null;
        _onMapMouseOut(clicked, mapId);

        if(clicked.attr("x") == element.attr("x") && clicked.attr("y") == element.attr("y"))
            return;
    }
    
    _onMapMouseMove(element, mapId);
    map._clickedAt = element
}

function _onMapMouseMove(element, mapId) {
    var map = $(mapId).data()

    if(map._clickedAt != null) {
        return;
    }

    map._onEnter.handle({map: map,
                         x: element.attr("x"),
                         y: element.attr("y")});

    var color = _toRgb(element.attr("default-color"));
    for(var i=0;i<3;i++)
        color[i] += 0.3;

    element.attr("bgcolor", _toHex(color))
    $(element.children()[0]).attr("style", "border: 1px solid black")
}

function _onMapMouseOut(element, mapId) {
    var map = $(mapId).data()

    if(map._clickedAt != null) return;

    map._onLeave.handle({x: element.attr("x"), y: element.attr("y")})

    element.attr("bgcolor", $(element).attr("default-color"))
    $(element.children()[0]).attr("style", "")
}
