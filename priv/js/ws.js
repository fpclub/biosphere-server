"use strict";

var MaxReconnects = 1; 

function _establish(conn) {
    conn.socket = new WebSocket(conn.address);

    conn.socket.onopen = function() {
        conn.state = "connected";
        conn.reconnects = 0;
        conn.errors = 0;

        conn.onConnect();
    };

    conn.socket.onmessage = function(data) {
        conn.onReceive($.parseJSON(data.data));
    }

    conn.socket.onerror = function(err) {
    }
    
    conn.socket.onclose = function(event) {
        conn.state = "disconnected";

        conn.reconnects = conn.reconnects + 1;
        if(conn.reconnects > MaxReconnects) {
            alert("Cannot connect to server");
            throw { message: "Connection error" };
        }
        
        _establish(conn);
    };
}

function _makeConnection(path, onConnect) {
    var address = "ws://" + BioServerPath + path;

    var conn = {
        address: address,
        socket: null,
        state: "disconnected",
        reconnects: 0,
        errors: 0,
        onConnect: onConnect,
        onReceive: function(data) { _wsOnReceive(data); },
        receivers: {}
    };

    _establish(conn);

    return conn;
}

var BioConnection

function wsConnect(onLoad) {
    BioConnection = _makeConnection("/play", onLoad);

    if(BioConnection.socket.readyState == 1 &&
       BioConnection.state != "connected")
        BioConnection.socket.onopen();
}

function wsHandle(type, callback) {
    if(BioConnection.receivers[type] == undefined) BioConnection.receivers[type] = makeEventHandler();
    BioConnection.receivers[type].push(callback);
}

function _wsOnReceive(data) {
    var receivers = BioConnection.receivers[data.type];
    if(receivers != undefined)
        receivers.handle(data);
}

function wsSend(data) {
    if(BioConnection.state != "connected")
        throw { message: "Socket disconnected" };

    BioConnection.socket.send(JSON.stringify(data));
}

function wsClose() {
    var socket = BioConnection.socket;
    socket.onclose = null;
    socket.close();
}
