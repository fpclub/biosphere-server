-module(bio_game_logic_SUITE).

-compile(export_all).
-include("bio_game_instance.hrl").
-include_lib("common_test/include/ct.hrl").

all() -> [growing_specie,
          migrating_specie].

%
% Case
%

init_per_testcase(_, Config) -> 
    Config.

end_per_testcase(_, Config) ->
    Config.

%
% Tests
%

growing_specie(_) ->
    Specie = #specie_state{name = something, number = 10.0},
    19.0 = bio_game_logic:grow_specie(Specie, #cell_state{}).

% growing depends on max_number in cell state (will be replaced by resources some day)
% growing with different breed
% growing several species

migrating_specie(_) ->
    Specie = #specie_state{name = something, number = 100.0},
    1.0 = bio_game_logic:migrate_specie(
            Specie,
            #cell_state{area_type = something},
            #cell_state{area_type = something}),
    0 = bio_game_logic:migrate_specie(
          Specie,
          #cell_state{area_type = something},
          #cell_state{area_type = seomthing_other}).
 

