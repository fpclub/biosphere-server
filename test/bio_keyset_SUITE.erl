-module(bio_keyset_SUITE).

-compile(export_all).
-include("bio_keyset.hrl").
-include_lib("common_test/include/ct.hrl").

all() -> [keyset_from_list,
          keyset_from_list_with_duplicates,
          keyset_from_list_with_duplicates_using_fold,
          inserting_to_keyset,
          updating_keyset,
          merging_keysets,
          merging_many_keysets,
          filtering_keysets,
          mapping_keysets].

%
% Case
%

init_per_testcase(_, Config) -> 
    Config.

end_per_testcase(_, Config) ->
    Config.

%
% Tests
%

keyset_from_list(_) ->
    Set = bio_keyset:from_list([{1, 2, 3}, {4, 5, 6}, {7, 8, 9}], 2),
    {1, 2, 3} = bio_keyset:find(2, Set),
    {4, 5, 6} = bio_keyset:find(5, Set),
    {7, 8, 9} = bio_keyset:find(8, Set).

keyset_from_list_with_duplicates(_) ->
    Set = bio_keyset:from_list([{1}, {2}, {1}, {3}, {2}, {3}, {4}, {2}],
                               1),
    [{1}, {2}, {3}, {4}] = bio_keyset:to_list(Set).

keyset_from_list_with_duplicates_using_fold(_) ->
    Source = [{1, 2}, {2, 1}, {1, 3}, {1, 5},
              {2, 4}, {3, 9}, {4, -1}, {3, 1}],
    Set = bio_keyset:from_list(Source, 1, fun({K, V1}, {K, V2}) ->
                                                  {K, V1 + V2}
                                          end),
    [{1, 10}, {2, 5}, {3, 10}, {4, -1}] = bio_keyset:to_list(Set).

inserting_to_keyset(_) ->
    Source = bio_keyset:from_list([{1, 2}, {2, 3}, {4, 5}, {6, 8}], 1),
    {3, 5} = bio_keyset:find(3, bio_keyset:insert({3, 5}, Source)),
    {'EXIT', {already_exists, _}} =
        (catch bio_keyset:insert({2, 2}, Source)),
    ok.

updating_keyset(_) ->
    Source = bio_keyset:from_list([{1, 2}, {2, 3}, {3, 4}], 1),
    Updated = bio_keyset:update(fun({K, X}) -> {K, X * X} end, 2, Source),
    [{1, 2}, {2, 9}, {3, 4}] = bio_keyset:to_list(Updated).

merging_keysets(_) ->
    S1 = bio_keyset:from_list([{3, 1}, {2, 4}, {1, 6}, {5, 18}], 1),
    S2 = bio_keyset:from_list([{2, -1}, {4, 16}, {1, 2}, {0, 5}], 1),
    Merged = bio_keyset:merge(fun({K, V1}, {K, V2}) ->
                                      {K, V1 + V2}
                              end, S1, S2),
    [{0, 5}, {1, 8}, {2, 3}, {3, 1}, {4, 16}, {5, 18}] =
        bio_keyset:to_list(Merged).

merging_many_keysets(_) ->
    S1 = bio_keyset:from_list([{2, 1}, {1, 2}], 1),
    S2 = bio_keyset:from_list([{3, 2}, {1, -1}], 1),
    S3 = bio_keyset:from_list([{4, -2}, {2, 5}], 1),
    S4 = bio_keyset:from_list([{3, 10}, {4, 3}], 1),
    Merged = bio_keyset:merge(fun({K, V1}, {K, V2}) -> {K, V1 + V2} end,
                              [S1, S2, S3, S4]),
    [{1, 1}, {2, 6}, {3, 12}, {4, 1}] = bio_keyset:to_list(Merged).

filtering_keysets(_) ->
    S = bio_keyset:from_list([{1, 2}, {2, 3}, {3, 4}, {4, 5}], 1),
    Filtered = bio_keyset:filter(fun({_, V}) -> V rem 2 == 0 end,
                                 S),
    [{1, 2}, {3, 4}] = bio_keyset:to_list(Filtered).

mapping_keysets(_) ->
    S = bio_keyset:from_list([{1, 2}, {2, 3}, {3, 4}], 1),
    Mapped = bio_keyset:map(fun({K, V}) -> {K, V * V} end, S),
    [{1, 4}, {2, 9}, {3, 16}] = bio_keyset:to_list(Mapped).
