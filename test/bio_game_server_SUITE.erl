-module(bio_game_server_SUITE).

-compile(export_all).
-include("bio_game_instance.hrl").
-include_lib("common_test/include/ct.hrl").

all() -> [setting_server_up,
          terminating,
          changing_code,
          stopping_server,
          handling_casts,
          handling_messages,
          making_game_instance,
          getting_all_instances,
%          getting_not_existing_instance,
          joining_instance,
%          trying_to_join_wrong_instance,
          leaving_instance,
%          destroying_instance,
          updating_instance,
          updating_destroyed_instance,
          forwarding_map_updates,
          adding_specie].

%
% Case
%

make_species(List) ->
    bio_keyset:from_list(List, #specie_state.name).

make_cell(List) ->
    #cell_state{species = make_species(List)}.

make_cell() -> make_cell([]).

init_per_testcase(_, Config) -> 
    bio_game_server:start_link(),
    bio_login_server:start_link(),
    meck:new(bio_game_instance, [passthrough]),
    meck:expect(
      bio_game_instance, init,
      fun() -> #instance_state{age = 0,
                               update_each=20,
                               map = bio_matrix:from_list(
                                       [[make_cell(),
                                         make_cell(),
                                         make_cell()],
                                        [make_cell(),
                                         make_cell(),
                                         make_cell()]])}
      end),
    meck:expect(bio_game_instance, join,
                fun(Name, Instance = #instance_state{players = Players}) ->
                        Instance#instance_state{players = maps:put(Name, [], Players)}
                end),
    meck:expect(bio_game_instance, update,
                fun(Instance = #instance_state{age = Age} ) ->
                        Instance#instance_state{age = Age + 100500}
                end),
    meck:expect(bio_game_instance, leave,
                fun(Name, Instance) -> Instance#instance_state{players=all_out} end),
    meck:expect(bio_game_instance, terminate, fun(Instance) -> ok end),
    Config.

end_per_testcase(_, Config) ->
    catch bio_game_server:stop(),
    meck:unload(bio_game_instance),
    Config.

%
% Generic tests
%

setting_server_up(_) ->
    {ok, #{}} = bio_game_server:init([]),
    ok.

terminating(_) ->
    ok = bio_game_server:terminate(normal, some_state).

changing_code(_) ->
    ok = bio_game_server:code_change(undef1, undef2, undef3).

stopping_server(_) ->
    shutdown_ok = bio_game_server:stop(),
    timer:sleep(10),
    {'EXIT', {noproc, _}} = (catch bio_game_server:stop()),
    ok.

handling_casts(_) ->
    {noreply, some_state} = bio_game_server:handle_cast(some_thing, some_state).

handling_messages(_) ->
    {noreply, some_state} = bio_game_server:handle_info(some_msg, some_state).

%
% Logic tests
%

making_game_instance(_) ->
    Id = bio_game_server:make_instance(),
    #instance_state{players = #{}} = bio_game_server:get_instance(Id).

getting_all_instances(_) ->
    Ids = [bio_game_server:make_instance() || _ <- lists:seq(1, 10)],
    Instances = lists:map(fun(Id) -> {Id, bio_game_server:get_instance(Id)} end, Ids),
    Instances = maps:to_list(bio_game_server:get_instances()).
    
getting_not_existing_instance(_) ->
    {'EXIT', {timeout, _}} = (catch bio_game_server:get_instance(youll_never_find_it)),
    ok.

joining_instance(_) ->
    Id = bio_game_server:make_instance(),
    #instance_state{players=#{"Chubaka" := []}} =
        bio_game_server:join_instance("Chubaka", Id),
    #instance_state{players=#{"Chubaka" := []}} =
        bio_game_server:get_instance(Id).

trying_to_join_wrong_instance(_) ->
    {'EXIT', {timeout, _}} =
        (catch bio_game_server:join_instance("Chubaka", millenium_falcon)),
    ok.

leaving_instance(_) ->
    Id = bio_game_server:make_instance(),
    ok = bio_game_server:leave_instance(2, Id),
    ok = bio_game_server:leave_instance(unknown, unknown),
    #instance_state{players=all_out} = bio_game_server:get_instance(Id).

destroying_instance(_) ->
    Id = bio_game_server:make_instance(),
    ok = bio_game_server:destroy_instance(Id),
    ok = bio_game_server:destroy_instance(unknown),
    {'EXIT', {timeout, _}} = (catch bio_game_server:get_instance(Id)),
    ok.

updating_instance(_) ->
    Id = bio_game_server:make_instance(),
    timer:sleep(22),
    #instance_state{age = 100500} = bio_game_server:get_instance(Id),
    timer:sleep(22),
    #instance_state{age = 201000} = bio_game_server:get_instance(Id),
    ok.

updating_destroyed_instance(_) ->
    bio_game_server:destroy_instance(bio_game_server:make_instance()),
    timer:sleep(12),
    ok.

forwarding_map_updates(_) ->
    Name = bio_login_server:login(),
    Id = bio_game_server:make_instance(),
    bio_game_server:join_instance(Name, Id),
    ok = receive
             {update, #instance_state{age = 100500}} -> ok
         after 22 -> error
         end.
                   
adding_specie(_) ->    
    Id = bio_game_server:make_instance(),
    Specie = #specie_state{name = <<"Sample">>, number = 100500},
    bio_game_server:add_specie(Specie, Id),
    #instance_state{map = Map} = bio_game_server:get_instance(Id),
    true = bio_matrix:any(fun(#cell_state{species = Species}) ->
                                  Species == make_species([Specie])
                          end, Map).

    
    
