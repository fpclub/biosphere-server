-module(bio_matrix_SUITE).

-compile(export_all).
-include("bio_matrix.hrl").
-include_lib("common_test/include/ct.hrl").

all() -> [making_matrix_from_list,
          merging_matrices,
          matrix_to_list,
          mapping_matrix,
          foreach_for_matrix,
          updating_matrix_field,
          matrix_any,
          matrix_neighbours].


%
% Case
%

init_per_testcase(_, Config) -> 
    Config.

end_per_testcase(_, Config) ->
    Config.

%
% Tests
%

making_matrix_from_list(_) ->
    Matrix = bio_matrix:from_list([[1, 2, 3],
                                   [2, 4, 6],
                                   [3, 6, 9],
                                   [4, 8, 12]]),
    #matrix{width = 3, height = 4} = Matrix,
    1 = bio_matrix:at(Matrix, 1, 1),
    6 = bio_matrix:at(Matrix, 2, 3),
    4 = bio_matrix:at(Matrix, 1, 4),
    12 = bio_matrix:at(Matrix, 3, 4).

merging_matrices(_) ->
    List1 = [[1, 2, 3],
             [4, 5, 6]],
    List2 = [[2, 3, 4],
             [5, 6, 7]],
    M1 = bio_matrix:from_list(List1),
    M2 = bio_matrix:from_list(List2),
    MR = bio_matrix:from_list([[3, 5, 7],
                               [9, 11, 13]]),
    MR = bio_matrix:merge(fun({X, Y}) -> X + Y end, M1, M2).
                                  
matrix_to_list(_) ->
    List = [[1, 2, 3], [4, 5, 6]],
    List = bio_matrix:to_list(bio_matrix:from_list(List)).

mapping_matrix(_) ->
    List = [[1, 2, 3], [4, 5, 6]],
    Mapped = bio_matrix:from_list([[2, 3, 4], [5, 6, 7]]),
    Mapped = bio_matrix:map(fun(X) -> X + 1 end,
                            bio_matrix:from_list(List)).

foreach_for_matrix(_) ->
    List = [[1, 2, 3], [4, 5, 6]],
    ok = bio_matrix:foreach(fun(X) -> self() ! X end,
                            bio_matrix:from_list(List)),
    lists:foreach(fun(X) ->
                          ok = receive X -> ok
                               after 0 -> error
                               end
                  end, lists:seq(1, 6)).

updating_matrix_field(_) ->
    List = [[1, 2, 3], [4, 5, 6]],
    Matrix = bio_matrix:update(fun(X) -> X + 1 end,
                               bio_matrix:from_list(List), 3, 2),
    7 = bio_matrix:at(Matrix, 3, 2).
                                       
matrix_any(_) ->
    M = bio_matrix:from_list([[1, 2, 3], [4, 5, 6]]),
    true = bio_matrix:any(fun(X) -> X rem 2 == 0 end, M),
    false = bio_matrix:any(fun(X) -> X rem 7 == 0 end, M).
                                   
matrix_neighbours(_) ->                                  
    M = bio_matrix:from_list([[1, 2, 3],
                              [4, 5, 6],
                              [7, 8, 9]]),
    M2 = bio_matrix:map_neighbours(fun(X, Ns) -> {X, Ns} end, M),
    [[{1, [{right, 2}, {down, 4}]},
      {2, [{right, 3}, {down, 5}, {left, 1}]},
      {3, [{down, 6}, {left, 2}]}],
     [{4, [{up, 1}, {right, 5}, {down, 7}]},
      {5, [{up, 2}, {right, 6}, {down, 8}, {left, 4}]},
      {6, [{up, 3}, {down, 9}, {left, 5}]}],
     [{7, [{up, 4}, {right, 8}]},
      {8, [{up, 5}, {right, 9}, {left, 7}]},
      {9, [{up, 6}, {left, 8}]}]] = bio_matrix:to_list(M2).

    

