-module(bio_game_instance_SUITE).

-compile(export_all).
-include("bio_matrix.hrl").
-include("bio_game_instance.hrl").
-include_lib("common_test/include/ct.hrl").

all() -> [initializing,
          joining_instance,
          updating_instance,
          getting_map_layers,
          adding_specie_to_instance,
          getting_specie_layer,
          updating_with_specie,
          migrating_species_to_new_area,
          migrating_species_to_existing_area].

%
% Case
%

init_per_testcase(_, Config) -> 
    meck:new(bio_game_logic),
    meck:expect(bio_game_logic, grow_specie,
                fun(#specie_state{number = N}, #cell_state{}) ->
                        N * 2 end),
    meck:expect(bio_game_logic, migrate_specie,
                fun(#specie_state{}, #cell_state{}, 
                    #cell_state{area_type = Type}) ->
                        if Type == closed -> 0;
                           true -> 1
                        end
                end),
    Config.

end_per_testcase(_, Config) ->
    meck:unload(bio_game_logic),
    Config.

%
% Tests
%

make_species(List) ->
    bio_keyset:from_list(List, #specie_state.name).

initializing(_) ->
    #instance_state{map = Map, age=0, update_each=1000} =
        bio_game_instance:init(),
    Matrix = Map,
    #matrix{width = Width, height = Height} = Matrix,
    true = Width > 0,
    true = Height > 0,
    Species = make_species([]),
    bio_matrix:foreach(
      fun(#cell_state{area_type = _,
                      temperature = _,
                      species = NewSpecies}) ->
              NewSpecies = Species, ok
      end, Matrix).

joining_instance(_) ->
    #instance_state{players = #{"Chubaka" := #player_state{}}} =
        bio_game_instance:join("Chubaka", #instance_state{}).

updating_instance(_) ->
    #instance_state{age = 1} = bio_game_instance:update(bio_game_instance:init()).

getting_map_layers(_) ->
    SampleMap = bio_matrix:from_list(
                  [[#cell_state{area_type = ground, temperature = 0},
                    #cell_state{area_type = water, temperature = 10}],
                   [#cell_state{area_type = water, temperature = 20},
                    #cell_state{area_type = ground, temperature = 30}]]),
    GroundLayer = #map_layer{name = <<"ground">>,
                             content = [[ground, water],
                                        [water, ground]]},
    TemperatureLayer = #map_layer{name = <<"temperature">>,
                                  content = [[0, 10], [20, 30]]},
    [] = bio_game_instance:map_layers(#instance_state{}, []),
    [GroundLayer] = bio_game_instance:map_layers(
                      #instance_state{map = SampleMap}, [<<"ground">>]),
    [TemperatureLayer, GroundLayer] =
        bio_game_instance:map_layers(
          #instance_state{map = SampleMap},
          [<<"temperature">>, <<"ground">>]).

make_cell(Species) ->
    #cell_state{species = make_species(Species)}.

adding_specie_to_instance(_) ->
    Map = bio_matrix:from_list([[make_cell([]), make_cell([])],
                                [make_cell([]), make_cell([])]]),
    Specie = #specie_state{name = "Sample", number = 10, breed = 1},
    #instance_state{map = NewMap} = 
        bio_game_instance:add_specie(#instance_state{map = Map}, 2, 1,
                                     Specie),
    Species = make_species([Specie]),
    #cell_state{species = Species} = bio_matrix:at(NewMap, 2, 1).

getting_specie_layer(_) ->
    Specie1 = #specie_state{name = <<"Sample1">>, number = 10},
    Specie2 = #specie_state{name = <<"Sample2">>, number = 20},
    Map = bio_matrix:from_list([[make_cell([]),
                                 make_cell([Specie1])],
                                [make_cell([Specie2]),
                                 make_cell([Specie1, Specie2])]]),
    [#map_layer{name = <<"Specie Sample1">>,
                content = [[0, 10], [0, 10]]}] =
        bio_game_instance:map_layers(#instance_state{map = Map},
                                     [<<"Specie Sample1">>]),
    [#map_layer{name = <<"Specie Sample2">>,
               content = [[0, 0],
                          [20, 20]]}] =
        bio_game_instance:map_layers(#instance_state{map = Map},
                                     [<<"Specie Sample2">>]).

updating_with_specie(_) ->
    Map = bio_matrix:from_list(
            [[make_cell([#specie_state{number = 10}])]]),
    Instance = #instance_state{map = Map, age = 0},
    #instance_state{map = NewMap} =
        bio_game_instance:update(Instance),
    [[NewCell]] = bio_matrix:to_list(NewMap),
    NewSpecies = make_species([#specie_state{number = 20}]),
    #cell_state{species = NewSpecies} = NewCell.

sample_specie_cell(Number) ->
    make_cell([#specie_state{name = sample, number = Number}]).

migrating_species_to_new_area(_) ->
    EmptyCell = make_cell([]),
    Cell = sample_specie_cell(10),
    MigratedCell = sample_specie_cell(1),
    RemainingCell = sample_specie_cell(16),
    Map = bio_matrix:from_list([[EmptyCell, EmptyCell, EmptyCell],
                                [EmptyCell, Cell, EmptyCell],
                                [EmptyCell, EmptyCell, EmptyCell]]),
    #instance_state{map = NewMapMatrix} =
        bio_game_instance:update(#instance_state{map = Map, age = 0}),
    NewMap = bio_matrix:to_list(NewMapMatrix),
    [[EmptyCell, MigratedCell, EmptyCell],
     [MigratedCell, RemainingCell, MigratedCell],
     [EmptyCell, MigratedCell, EmptyCell]] = NewMap.

migrating_species_to_existing_area(_) ->
    Map = bio_matrix:from_list([[make_cell([]),
                                 make_cell([])],
                                [sample_specie_cell(10),
                                 sample_specie_cell(10)]]),
    #instance_state{map = NewMapMatrix} = 
        bio_game_instance:update(#instance_state{map = Map, age = 0}),
    NewMap = bio_matrix:to_list(NewMapMatrix),
    ExpectedMap = [[sample_specie_cell(1), sample_specie_cell(1)],
                   [sample_specie_cell(19), sample_specie_cell(19)]],
    ExpectedMap = NewMap.

%zero isn't a migration
