-module(bio_server_sup_SUITE).

-compile(export_all).
-include_lib("common_test/include/ct.hrl").

all() -> [starting_login_server].

init_per_testcase(_, Config) -> 
    Config.

end_per_testcase(_, Config) ->
    Config.

starting_login_server(_) ->
    bio_server_sup:start_link(),
    Pid = whereis(bio_login_server),
    true = is_pid(Pid).

    
