-module(bio_ws_handler_SUITE).

-compile(export_all).
-include_lib("common_test/include/ct.hrl").

all() -> [initialization,
          websocket_initialization,
          websocket_lazy_initialization,
          termination,
          handling_no_text_requests,
          handling_no_json_requests,
          handling_json_without_type_requests,
          handling_json,
          handling_messages].

init_per_testcase(_, Config) ->
    meck:new(bio_protocol, [unstick, passthrough]),
    meck:expect(bio_protocol, handle,
                fun(State, Msg) ->
                        case Msg of
                            [] -> {noreply, state1};
                            _ -> {reply, #{type => Msg}, state2}
                        end
                end),
    meck:expect(bio_protocol, handle,
                fun(State, _, _) -> {#{some => data}, State} end),
    meck:expect(bio_protocol, init, fun() -> some_usefull_data end),
    meck:expect(bio_protocol, terminate, fun(State) -> {ok, State} end),
    Config.

end_per_testcase(_, Config) ->
    meck:unload(bio_protocol),
    Config.
          
%
% Setup and termination
%

initialization(_) ->
    {upgrade, protocol, cowboy_websocket} = bio_ws_handler:init(undef_1, undef_2, undef_3).

websocket_initialization(_) ->
    {ok, request, no_data} =
        bio_ws_handler:websocket_init(undef_1, request, undef_2),
    receive
        lazy_init -> ok
    after
        1000 -> erlang:error(timeout)
    end.

websocket_lazy_initialization(_) ->
    {ok, req, some_usefull_data} =
        bio_ws_handler:websocket_info(lazy_init, req, something).

termination(_) ->
    {ok, some_data} =
        bio_ws_handler:websocket_terminate(undef_1, undef_2, some_data).

%
% Handlers
%

reply_error(Error, Req, State) ->
    {reply, {text, <<"{\"type\":\"error\",\"error\":\"", Error/binary, "\"}">>}, Req, State}.

handling_no_text_requests(_) ->
    Got = bio_ws_handler:websocket_handle({something, some}, req, state),
    Got = reply_error(<<"Invalid request">>, req, state).

handling_no_json_requests(_) ->
    Got = bio_ws_handler:websocket_handle({text, <<"blabla">>}, req, state),
    Got = reply_error(<<"Invalid JSON">>, req, state).

handling_json_without_type_requests(_) ->
    Got = bio_ws_handler:websocket_handle({text, <<"{}">>}, req, state),
    Got = reply_error(<<"Request has no type">>, req, state). 

handling_json(_) ->
    Got = bio_ws_handler:websocket_handle({text , <<"{\"type\":\"ping\"}">>}, req, state),
    {reply, {text, <<"{\"some\":\"data\"}">>}, req, state} = Got.

handling_messages(_) ->
    {ok, request, state1} = 
        bio_ws_handler:websocket_info([], request, state),
    {reply, {text, <<"{\"type\":\"hi\"}">>}, request, state2} =
        bio_ws_handler:websocket_info(hi, request, state).
