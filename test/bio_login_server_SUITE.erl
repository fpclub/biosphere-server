-module(bio_login_server_SUITE).

-compile(export_all).
-include("bio_login_server.hrl").
-include_lib("common_test/include/ct.hrl").

all() -> [setting_server_up,
          logging_in,
          logging_in_several_users,
          receiving_logged_in_users,
          logging_out,
          forwarding_messages_to_users,
          stopping_server,
          terminating,
          changing_code,
          handling_info,
          removing_dead_threads].

init_per_testcase(_, Config) -> 
    bio_login_server:start_link(),
    Config.

end_per_testcase(_, Config) ->
    Config.

setting_server_up(_) ->
    {ok, #login_server_state{}} = bio_login_server:init([]),
    {'EXIT', _} = (catch bio_login_server:init(some_info)),
    ok.

logging_in(_) ->
    <<"Anonymous 1">> = bio_login_server:login().

logging_in_several_users(_) ->
    <<"Anonymous 1">> = bio_login_server:login(),
    <<"Anonymous 2">> = bio_login_server:login().

receiving_logged_in_users(_) ->
    Name1 = bio_login_server:login(),
    Name2 = bio_login_server:login(),
    Pid = self(),
    Users = maps:put(Name2, Pid, maps:put(Name1, Pid, maps:new())),
    Users = bio_login_server:logged_in().

logging_out(_) ->
    Name = bio_login_server:login(),
    bio_login_server:logout(Name),
    bio_login_server:forward(Name, hi),
    ok = receive hi -> error
         after 10 -> ok
         end.
                                   
forwarding_messages_to_users(_) ->
    Name = bio_login_server:login(),
    bio_login_server:forward(Name, some_message),
    ok = receive some_message -> ok
         after 10 -> error
         end.

terminating(_) ->
    ok = bio_login_server:terminate(normal, undef_state),
    {'EXIT', _} = (catch bio_login_server:terminate(ooops, undef_state)),
    ok.

changing_code(_) ->
    ok = bio_login_server:code_change(undef1, undef2, undef3).

handling_info(_) ->
    {noreply, some_state} = bio_login_server:handle_info(some_message, some_state).
    
stopping_server(_) ->
    shutdown_ok  = bio_login_server:stop(),
    {'EXIT', {noproc, _}} = (catch bio_login_server:stop()),
    ok.

removing_dead_threads(_) ->
    Pid = erlang:spawn(bio_login_server, login, []),
    Ref = erlang:monitor(process, Pid),
    ok = receive
             {'DOWN', Ref, process, Pid, _} -> 
                 timer:sleep(10),
                 [] = maps:to_list(bio_login_server:logged_in()),
                 ok
         after 1000 -> timeout
         end.
                 

            
