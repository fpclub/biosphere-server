-module(bio_protocol_SUITE).

-compile(export_all).
-include("bio_protocol.hrl").
-include("bio_game_instance.hrl").
-include_lib("common_test/include/ct.hrl").

all() -> [wrong_request_type_response,
          protocol_initialization,
          protocol_termination,
          ping_message_response,
          requesting_map_layers,
          requesting_player_name,
          receiving_players_instance,
          handling_updates,
          getting_instance].

mock_login_server() ->
    meck:new(bio_login_server),
    meck:expect(bio_login_server, login, fun() -> <<"Some name">> end),
    meck:expect(bio_login_server, logout, fun(Name) -> self() ! {logged_out, Name}, ok end).

mock_game_server() ->
    meck:new(bio_game_server),
    meck:expect(bio_game_server, make_instance, fun() -> some_id end),
    meck:expect(bio_game_server, join_instance,
                fun(<<"Some name">>, some_id) -> self() ! joined
                end),
    meck:expect(bio_game_server, get_instance,
                fun(some_id) ->
                        #instance_state{map=[[#cell_state{area_type = water, temperature = 0},
                                              #cell_state{area_type = ground, temperature = 10}],
                                             [#cell_state{area_type = ground, temperature = 20},
                                              #cell_state{area_type = water, temperature = 30}]]}
                end),
    meck:expect(bio_game_server, add_specie,
                fun(#specie_state{name = <<"Sample">>, number = 10}, some_id) ->
                        self() ! new_specie, ok end),
    meck:expect(bio_game_server, destroy_instance,
                fun(some_id) -> self() ! destroyed, ok end).

mock_game_instance() ->
    meck:new(bio_game_instance),
    meck:expect(bio_game_instance, map_layers,
                fun(#instance_state{}, Layers) ->
                        [#map_layer{name = N, content = C} ||
                            {N, C} <- lists:zip(Layers, lists:seq(1, length(Layers)))]
                end).

init_per_testcase(_, Config) ->
    mock_login_server(),
    mock_game_server(),
    mock_game_instance(),
    Config.

end_per_testcase(_, Config) ->
    meck:unload(bio_login_server),
    meck:unload(bio_game_server),
    meck:unload(bio_game_instance),
    Config.
          
protocol_initialization(_) ->
    #bio_state{name = <<"Some name">>, instance = some_id} =
        bio_protocol:init(),
    ok = receive joined -> ok
         after 0 -> error
         end,
    ok = receive new_specie -> ok
         after 0 -> error
         end.

protocol_termination(_) ->
    ok = bio_protocol:terminate(#bio_state{name = some_name, instance = some_id}),
    ok = receive
             destroyed ->
                 receive
                     {logged_out, some_name} -> ok
                 after 0 ->
                         error
                 end
         after 0 ->
                 error
         end.
                    
wrong_request_type_response(_) ->
    {#{type := error}, some_state} =
        bio_protocol:handle(some_state,
                            <<"you'll never catch me">>,
                            #{error => <<"Unknown request type: you'll never catch me">>}).

ping_message_response(_) ->
    {#{type := pong}, some_state} =
        bio_protocol:handle(some_state, <<"ping">>, #{}).

requesting_map_layers(_) ->
    Layers = [<<"family">>, <<"friends">>, <<"fools">>],
    {#{type := map,
       layers := [#{name := <<"family">>, content := 1},
                  #{name := <<"friends">>, content := 2},
                  #{name := <<"fools">>, content := 3}]},
     #bio_state{}} =
        bio_protocol:handle(#bio_state{instance = some_id},
                            <<"require">>, #{<<"what">> => <<"map">>,
                                             <<"layers">> => Layers}).
 
requesting_player_name(_) ->
    {#{type := name, name := "a name"}, #bio_state{name = "a name"}} =
        bio_protocol:handle(#bio_state{name = "a name"}, 
                            <<"require">>, #{<<"what">> => <<"name">>}).

receiving_players_instance(_) ->
    State = #bio_state{instance = some_id},
    Ref = make_ref(),
    {noreply, State} = bio_protocol:handle(State, {get_instance, self(), Ref}),
    ok = receive {Ref, some_id} -> ok
         after 0 -> timeout
         end.

handling_updates(_) ->
    {reply, #{type := update, age := 42}, some_state} =
        bio_protocol:handle(some_state,
                            {update, #instance_state{age = 42}}).
    
getting_instance(_) ->
    Pid = spawn(fun() ->
                        receive
                            {get_instance, Pid, Ref} -> Pid ! {Ref, 100500}
                        end
                end),
    100500 = bio_protocol:get_instance(Pid).
   



                                 
                                 
                             
                            
                        
